(function(){
  var BASE_URL = "http://81.2.241.234:8080";

  var config = {
    backend: {
      hero: BASE_URL + "/hero",
      species: BASE_URL + "/species",
    }
  }

  var heroPanel = new Vue({
    el: '#hero',
    created: function() {
      this.refreshItems();
    },
    data: {
      items: []
    },
    methods: {
      refreshItems: function() {
        var self = this;
        axios.get(config.backend.hero).then(function(res) {
          self.items = res.data;
        });
      },
      deleteById: function(id) {
        var self = this;
        axios.delete(config.backend.hero + "/" + id).then(function(res) {
          self.refreshItems();
        });
      },
      prepareItem: function(id, name, desc) {
        modal.updateFields(id || null, name || null, desc || null, config.backend.hero);
      }
    }
  });

  var speciesPanel = new Vue({
    el: '#species',
    created: function() {
      this.refreshItems();
    },
    data: {
      items: []
    },
    methods: {
      refreshItems: function() {
        var self = this;
        axios.get(config.backend.species).then(function(res) {
          self.items = res.data;
        });
      },
      deleteById: function(id) {
        var self = this;
        axios.delete(config.backend.species + "/" + id).then(function(res) {
          self.refreshItems();
        });
      },
      prepareItem: function(id, name, desc) {
        modal.updateFields(id || null, name || null, desc || null, config.backend.species);
      }
    }
  });

  var modal = new Vue({
    el: '#modal',
    data: {
      id: null,
      name: null,
      desc: null,
      endpoint: null,
      title: null
    },
    methods: {
      resetFields: function() {
        this.id = null;
        this.name = null;
        this.desc = null;
        this.endpoint = null;
        this.title = null;
      },
      updateFields: function(id, name, desc, endpoint) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.endpoint = endpoint;

        var entityType = endpoint.substring(endpoint.lastIndexOf("/") + 1);

        if (this.id === null) {
          this.title = "Add new " + entityType;
        } else {
          this.title = "Update " + entityType;
        }
      },
      send: function() {
        var self = this;
        var options = {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
        };

        var body = "desc=" + this.desc + "&name=" + encodeURIComponent(this.name);

        if (this.id !== null) {
          axios.put(this.endpoint + "/" + this.id, body, options).then(function(res) {
            heroPanel.refreshItems()
            speciesPanel.refreshItems()
          });
        } else {
          axios.post(this.endpoint, body, options).then(function(res) {
            heroPanel.refreshItems()
            speciesPanel.refreshItems()
          });
        }
      }
    }
  })

  $('#modal').on('hidden.bs.modal', function () {
    modal.resetFields();
  })
})();
